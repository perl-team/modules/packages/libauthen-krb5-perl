Source: libauthen-krb5-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Russ Allbery <rra@debian.org>,
           Ansgar Burchardt <ansgar@debian.org>,
           gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               perl-xs-dev,
               perl:native,
               libkrb5-dev
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libauthen-krb5-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libauthen-krb5-perl.git
Homepage: https://metacpan.org/release/Krb5

Package: libauthen-krb5-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: Perl interface to Kerberos 5 API
 Authen::Krb5 is a Perl module providing an object-oriented interface to the
 Kerberos 5 API. It rearranges the API slightly to provide an object-oriented
 view, but otherwise closely matches the C interface. Use may require previous
 experience with Kerberos 5 programming or reference to the Kerberos 5 API
 documentation.
 .
 This package is built against the MIT Kerberos 5 libraries.
